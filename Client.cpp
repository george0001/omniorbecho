#include <iostream>
#include <csignal>
#include <termios.h>

#include "generated/Consumer.hh"

auto main(int argc, char* argv[]) -> int {
    if (argc != 2) {
        std::cerr << "usage:  client <object reference>" << std::endl;

        return 1;
    }

    try {
        static CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
        CORBA::Object_var obj = orb->string_to_object(argv[1]);

        Consumer_var consumer = Consumer::_narrow(obj);

        if (CORBA::is_nil(consumer)) {
            std::cerr << "Can't narrow reference to type Consumer (or it was nil)." << std::endl;

            return 1;
        }

        static struct termios oldTerminalSettings;

        if (tcgetattr(0, &oldTerminalSettings) == -1) {
            std::cerr << "Unable to read the terminal settings." << std::endl;

            return 1;
        }

        auto newTerminalSettings = oldTerminalSettings;

        newTerminalSettings.c_lflag &= ~ICANON;
        newTerminalSettings.c_cc[VTIME] = 0;
        newTerminalSettings.c_cc[VMIN] = 1;

        if (tcsetattr(0, TCSANOW, &newTerminalSettings) == -1) {
            std::cerr << "Unable to adjust the terminal settings." << std::endl;

            return 1;
        }

        signal(SIGINT, [] (int signum) {
            if (tcsetattr(0, TCSANOW, &oldTerminalSettings) == -1) {
                std::cerr << "Unable to restore the terminal settings." << std::endl;
            }

            orb->destroy();

            exit(signum);
        });

        while (true) {
            char c;
            std::cin.get(c);

            consumer->consume(c);
        }
    }
    catch (const CORBA::TRANSIENT&) {
        std::cerr << "Caught system exception TRANSIENT -- unable to contact the "
                  << "server." << std::endl;
    }
    catch (const CORBA::SystemException& ex) {
        std::cerr << "Caught a CORBA::" << ex._name() << std::endl;
    }
    catch (const CORBA::Exception& ex) {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
    }
}
