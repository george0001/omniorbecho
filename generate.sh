#!/usr/bin/env bash

top=$(pwd) && gen="./generated" && mkdir -p "${gen}" 2>/dev/null; (cd "${gen}" && omniidl -bcxx -Wbh=.hh -Wbs=SK.cc -Wbtp "${top}/Consumer.idl")
