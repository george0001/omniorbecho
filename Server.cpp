#include <iostream>

#include "generated/Consumer.hh"

class ConsumerImpl : public POA_Consumer {
    public:
        inline ConsumerImpl() {}
        virtual ~ConsumerImpl() {}
        virtual void consume(CORBA::Char c);
};


void ConsumerImpl::consume(CORBA::Char c) {
    std::cout << c << std::flush;
}

auto main(int argc, char* argv[]) -> int {
    try {
        CORBA::ORB_var orb = CORBA::ORB_init(argc, argv);
        CORBA::Object_var rootPoa = orb->resolve_initial_references("RootPOA");
        PortableServer::POA_var serverPoa = PortableServer::POA::_narrow(rootPoa);
        PortableServer::Servant_var<ConsumerImpl> consumerImpl = new ConsumerImpl();
        PortableServer::ObjectId_var consumerId = serverPoa->activate_object(consumerImpl);
        Consumer_var consumer = consumerImpl->_this();
        const CORBA::String_var url = orb->object_to_string(consumer);
        std::cout << url << std::endl;
        PortableServer::POAManager_var poaManager = serverPoa->the_POAManager();
        poaManager->activate();
        orb->run();
    }
    catch (const CORBA::SystemException& ex) {
        std::cerr << "Caught CORBA::" << ex._name() << std::endl;
    }
    catch (const CORBA::Exception& ex) {
        std::cerr << "Caught CORBA::Exception: " << ex._name() << std::endl;
    }
}
